from db import session, User

def is_admin(message):
    username = message.from_user.username
    users_found = session.query(User).filter(User.username==username).all()
    if len(users_found) and users_found[0].is_admin:
        return True
    return False