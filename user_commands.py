import re
import telebot
from services import is_admin

from db import session, User, Course, Video, User_to_Course

from bot import bot, user_states, config


@bot.message_handler(commands=["courses"])
def get_user_courses(message):
    chat_id = message.chat.id
    username = message.from_user.username
    users_found = session.query(User).filter(User.username == username).all()
    if not len(users_found):
        bot.send_message(chat_id, "Ваш аккаунт не найден, сначала зарегистрируйтесь написав /start")
        return
    courses_found = session.query(Course).all()
    if not len(courses_found):
        bot.send_message(chat_id, "Пока доступных курсов нет.")
        return
    courses = ["курс:\n\n" + c.course_name + "\n\n" + "описание:\n\n" + c.course_description for c in
     courses_found]


    for course in courses:
        bot.send_message(chat_id, course)

    btns = []
    for course in courses_found:
        btns.append(telebot.types.InlineKeyboardButton(course.course_name, callback_data=f"demo#{course.course_name}"))
    markup = telebot.types.InlineKeyboardMarkup([btns])
    bot.send_message(chat_id,
                     """чтобы увидеть пробную версию курса выберите её из опций\n\nчтобы приобрести доступ к полной \
                     версии курсов перейдите в магазин написав\n/shop""",
                     reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == "seeCourses")
def get_user_courses_callback(call):
    chat_id = call.message.chat.id

    users_found = session.query(User).filter(User.chatID == chat_id).all()
    if not len(users_found):
        bot.send_message(chat_id, "Ваш аккаунт не найден, сначала зарегистрируйтесь написав /start")
        return
    courses_found = session.query(Course).all()
    if not len(courses_found):
        bot.send_message(chat_id, "Пока доступных курсов нет.")
        return
    courses = ["курс:\n\n" + c.course_name + "\n\n" + "описание:\n\n" + c.course_description for c in
     courses_found]


    for course in courses:
        bot.send_message(chat_id, course)

    btns = []
    for course in courses_found:
        btns.append(telebot.types.InlineKeyboardButton(course.course_name, callback_data=f"demo#{course.courseID}"))
    markup = telebot.types.InlineKeyboardMarkup([btns])
    bot.send_message(chat_id,
                     """чтобы увидеть пробную версию курса выберите её из опций\n\nчтобы приобрести доступ к полной \
                     версии курсов перейдите в магазин написав\n/shop""",
                     reply_markup=markup)



@bot.callback_query_handler(func=lambda call: len(call.data.split("#")) and call.data.split("#")[0]=="demo")
def request_demo(call):
    chat_id = call.message.chat.id
    course_id = call.data.split("#")[1]
    courses_found = session.query(Course).filter(Course.courseID == course_id).all()
    if not len(courses_found):
        bot.send_message(chat_id, "произошла ошибка, курс не найден попробуйте снова.")
        return
    course = courses_found[0]
    course_name = course.course_name

    videos = session.query(Video).filter(Video.courseID == course.courseID).all()
    if not len(videos):
        bot.send_message(chat_id, "у этого курса пока нет пробной версии, попробуйте позже.")
        return
    bot.send_message(chat_id, course.course_description)
    for video in videos:
        bot.send_video(chat_id, video=video.video_route)

    markup = telebot.types.ReplyKeyboardMarkup()
    btn1 = telebot.types.KeyboardButton("/shop")
    btn2 = telebot.types.KeyboardButton("/courses")
    markup.row(btn1)
    markup.row(btn2)

    bot.send_message(chat_id, "Понравился курс?\nВы можете приобрести полную версию перейдя в магазин написав  /shop\nВы можете ознакомиться с пробными версиями других курсов написав  /courses", reply_markup=markup)




@bot.message_handler(commands=["shop"])
def see_shop(message):
    chat_id = message.chat.id

    courses_found = session.query(Course).all()
    print(courses_found)
    for course in courses_found:
        bot.send_invoice(chat_id,
                         provider_token=config["PAYMENTS_TOKEN"],
                         title=course.course_name,
                         description=f"о курсе: {course.course_description}\n\nпосле покупки вы получите ссылку на материалы курса.",
                         currency="EUR",
                         invoice_payload=f"payment_received#{course.course_name}#{message.from_user.username}",
                         prices=[telebot.types.LabeledPrice(course.course_name, course.course_price)],
                         )

    if not len(courses_found):
        bot.send_message(chat_id, "В продаже пока нет курсов.")

@bot.callback_query_handler(func=lambda call: call.data == "buyCourses")
def see_shop_callback(call):
    chat_id = call.message.chat.id


    courses_found = session.query(Course).all()
    print(courses_found)
    for course in courses_found:
        bot.send_invoice(chat_id,
                         provider_token=config["PAYMENTS_TOKEN"],
                         title=course.course_name,
                         description=f"о курсе: {course.course_description}\n\nпосле покупки вы получите ссылку на материалы курса.",
                         currency="EUR",
                         invoice_payload=f"payment_received#{course.course_name}#{chat_id}",
                         prices=[telebot.types.LabeledPrice(course.course_name, course.course_price)],
                         )

    if not len(courses_found):
        bot.send_message(chat_id, "В продаже пока нет курсов.")



@bot.callback_query_handler(func=lambda call: len(call.data.split("#")) and call.data.split("#")[0]=="payment_received")
def handle_payment_received(call):
    data = call.data.split("#")
    users_found = session.query(User).filter(User.chatID == data[2]).all()
    if not len(users_found):
        bot.send_message(call.message.chat.id, "Произошла ошибка, ваш аккаунт не найден. Если вы оплатили курс, не получили ссылку и видите эту ошибку, обратитесь к администратору написав /contact\nПриносим извинения за неудобства.")
    user = users_found[0]
    course = session.query(Course).filter(Course.course_name == data[1]).all()
    if not len(users_found):
        bot.send_message(call.message.chat.id, "Произошла ошибка, курс не найден. Если вы оплатили курс, не получили ссылку и видите эту ошибку, обратитесь к администратору написав /contact\nПриносим извинения за неудобства.")

    user_to_course = User_to_Course(user.userID, course.courseID)
    user_to_course.has_paid = True
    session.add(user_to_course)
    session.commit()

    bot.send_message(call.message.chat.id, f"спасибо за оплату,\nматериалы курса доступны по этой ссылке:\n{course.course_access_url}\n\nВ случае проблем обращайтесть к администраторам, чьи контакты доступны по комманде /contact")

@bot.message_handler(commands=["contact"])
def get_user_courses(message):
    chat_id = message.chat.id
    bot.send_message(chat_id, "Контакты наших админов:\n\nАнтонина - +48537319832\n\nвсе наши админы также есть в Телеграмме")

@bot.callback_query_handler(func=lambda call: len(call.data.split("#")) and call.data.split("#")[0]=="getCourse")
def request_demo(call):
    chat_id = call.message.chat.id
    courses_found = session.query(Course).all()
    if not len(courses_found):
        return
    course = courses_found[0]

    btn1 = telebot.types.InlineKeyboardButton("приобрести курс", callback_data="buyCourses")
    markup = telebot.types.InlineKeyboardMarkup([[btn1]])

    bot.send_message(chat_id, f'{course.course_name}\n\nОписание:\n{course.course_description}', reply_markup=markup)





    #bot.send_message(chat_id, "Понравился курс?\nВы можете приобрести полную версию перейдя в магазин написав  /shop\nВы можете ознакомиться с пробными версиями других курсов написав  /courses", reply_markup=markup)
