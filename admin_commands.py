import re
import telebot
from services import is_admin

from db import session, User, Course, Video

from bot import bot, user_states


@bot.message_handler(commands=['admin'])
def get_admin_panel(message):
    print("PRINT5")
    chat_id = message.chat.id
    if not is_admin(message):
        bot.send_message(chat_id, "только администраторам доступна эта комманда")
        return
    bot.send_message(chat_id, "Здравствуйсте, Администратор, как я могу помочь вам сегодня?")
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2)
    itembtn1 = telebot.types.KeyboardButton('мои курсы')
    itembtn2 = telebot.types.KeyboardButton('добавить курс')
    itembtn3 = telebot.types.KeyboardButton('пользователи')
    markup.add(itembtn1, itembtn2, itembtn3)
    bot.send_message(chat_id, "выберите опцию:", reply_markup=markup)


@bot.message_handler(func=lambda msg: msg.text.lower() == "мои курсы" and is_admin(msg))
def get_сourses(message):
    print("PRINT6")
    chat_id = message.chat.id
    courses = [c.course_name + " | цена:" + str(c.course_price)[:-2] + "," + str(c.course_price)[-2:] for c in session.query(Course).all()]
    if len(courses):
        bot.send_message(chat_id, '\n'.join(courses))
        bot.send_message(chat_id, 'чтобы просмотреть детали курса и видео в нём напишите "детали курса <название курса>"')
    else:
        bot.send_message(chat_id, "у вас пока нет курсов.")

        markup = telebot.types.ReplyKeyboardMarkup(row_width=2)
        itembtn1 = telebot.types.KeyboardButton('добавить курс')
        itembtn2 = telebot.types.InlineKeyboardButton('панель админа', callback_data="/admin")
        markup.add(itembtn1, itembtn2)
        bot.send_message(chat_id, "выберите действие", reply_markup=markup)


@bot.message_handler(regexp="(?<=детали\sкурса\s).*$", func=lambda msg: is_admin(msg))
def get_course_details(message):
    print("PRINT7")
    chat_id = message.chat.id
    course_name = re.findall(r"(?<=детали\sкурса\s).*$", message.text, re.IGNORECASE)[0]
    course = session.query(Course).filter(Course.course_name == course_name).all()
    if not len(course):
        bot.send_message(chat_id, "курс не найден попробуйте снова")
        return
    videos = session.query(Video).filter(Video.courseID == course[0].courseID).order_by(Video.order_in_course).all()

    if not len(videos):
        bot.send_message(chat_id, f"{course_name}\n" + f"описание:\n{course[0].course_description} \n" + "\nВ этом курсе пока нет видео")
    else:
        bot.send_message(chat_id, f"{course_name}\n" + f"описание:\n{course[0].course_description}\n\n" + "\n".join([str(v.order_in_course) + " - " + v.video_name for v in videos]))

    inlinebtn = telebot.types.InlineKeyboardButton("изменить ссылку", callback_data=f"change_link#{course[0].courseID}")
    inline_markup = telebot.types.InlineKeyboardMarkup([[inlinebtn]])

    bot.send_message(chat_id, f"\n\nполный курс доступен по ссылке: {str(course[0].course_access_url) if course[0].course_access_url else 'ссылка отсутствует'}", reply_markup=inline_markup)


    markup = telebot.types.ReplyKeyboardMarkup(row_width=2)
    itembtn1 = telebot.types.KeyboardButton(f'добавить видео {course_name}')
    itembtn2 = telebot.types.KeyboardButton('/admin')
    itembtn3 = telebot.types.KeyboardButton('мои курсы')
    markup.add(itembtn1, itembtn2, itembtn3)
    bot.send_message(chat_id, "выберите действие", reply_markup=markup)


@bot.message_handler(regexp="(?<=добавить\sвидео\s).*$", func=lambda msg: is_admin(msg))
def request_adding_new_video(message):
    print("PRINT8")
    chat_id = message.chat.id
    bot.send_message(chat_id, "отправьте название и порядковый номер видео которое вы хотите добавить в формате: [название видео] [номер под которым оно будет в курсе]")
    user_states[chat_id] = ("newVideoDescExpected", re.findall(r"(?<=добавить\sвидео\s).*$", message.text, re.IGNORECASE)[0])


@bot.message_handler(func=lambda msg: msg.chat.id in user_states and user_states[msg.chat.id][0] == "newVideoDescExpected" and is_admin(msg))
def add_new_video_desc(message):
    print("PRINT13")
    chat_id = message.chat.id

    matches = re.findall(r'\[(.*?)\]', message.text)
    if len(matches) != 2 or any([not len(m) for m in matches]) or not matches[1].isnumeric() or int(matches[1]) != float(matches[1]):

        markup = telebot.types.ReplyKeyboardMarkup()
        button1 = telebot.types.KeyboardButton(f"добавить видео {user_states[chat_id][1]}")
        button2 = telebot.types.KeyboardButton("/admin")
        markup.add(button1, button2)
        bot.send_message(chat_id, 'неправильный формат, начните сначала', reply_markup=markup)
        user_states.pop(chat_id)
        return

    bot.send_message(chat_id, "теперь отправьте само видео")
    print(matches)
    user_states[chat_id] = ("newVideoExpected", [user_states[chat_id][1], matches])


@bot.message_handler(content_types=["video"], func=lambda msg: msg.chat.id in user_states and user_states[msg.chat.id][0] == "newVideoExpected" and is_admin(msg))
def add_new_video(message):
    print("PRINT9")
    chat_id = message.chat.id
    course_name = user_states[chat_id][1][0]
    video_name = user_states[chat_id][1][1][0]
    video_order = int(user_states[chat_id][1][1][1])
    user_states.pop(chat_id)
    video_info = bot.get_file(message.video.file_id)
    video_url = f"https://api.telegram.org/file/bot{bot.token}/{video_info.file_path}"
    print(message.text, message.video.file_id, video_url)
    try:
        course = session.query(Course).filter(Course.course_name == course_name).all()[0]

        for v in reversed(session.query(Video).filter(Video.courseID == course.courseID and Video.order_in_course >= video_order).all()):
            v.order_in_course = v.order_in_course + 1
        session.commit()

        video = Video(video_name, message.video.file_id, course.courseID, video_order)
        session.add(video)
        session.commit()

        markup = telebot.types.ReplyKeyboardMarkup(row_width=2)
        itembtn1 = telebot.types.InlineKeyboardButton('/admin', callback_data="/admin")
        itembtn2 = telebot.types.InlineKeyboardButton('мои курсы')
        markup.add(itembtn1, itembtn2)
        bot.send_message(chat_id, f"видео {video_name} успешно добавлено", reply_markup=markup)

    except:
        bot.send_message(chat_id, "что-то пошло не так, попробуйте начать сначала")



@bot.message_handler(func=lambda msg: msg.text.lower() == "добавить курс" and is_admin(msg))
def request_adding_new_course(message):
    print("PRINT10")
    chat_id = message.chat.id
    bot.send_message(chat_id, "напишите название курса, цену в Евро и описние в следующем формате (включая скобки): \n [название] [цена] [описание] \n (цену укажите без точки то есть вместо 12.99 просто 1299, обязательно 4-х значным числом)")
    user_states[chat_id] = ("newCourseDetailsExpected", "")


@bot.message_handler(func=lambda msg: msg.chat.id in user_states and user_states[msg.chat.id][0] == "newCourseDetailsExpected" and is_admin(msg))
def create_new_course(message):
    print("PRINT11")
    chat_id = message.chat.id
    user_states.pop(chat_id)
    matches = re.findall(r'\[(.*?)\]', message.text, re.DOTALL)
    if len(matches)!=3 or any([not len(m) for m in matches]) or not matches[1].isnumeric() or int(matches[1]) != float(matches[1]):
        bot.send_message(chat_id, 'Неправильный формат начните сначала написав "добавить курс"')
        return
    course = Course(matches[0], int(matches[1]), matches[2])
    session.add(course)
    session.commit()
    bot.send_message(chat_id, f"курс {matches[0]} успешно добавлен.")

@bot.callback_query_handler(func=lambda call: len(call.data.split("#")) and call.data.split("#")[0]=="change_link")
def request_adding_course_link(call):
    chat_id = call.message.chat.id
    user_states[chat_id] = ("change_link", call.data.split("#")[1])
    bot.send_message(chat_id, "Введите новую ссылку")

@bot.message_handler(func=lambda msg: msg.chat.id in user_states and len(user_states[msg.chat.id]) and user_states[msg.chat.id][0]=="change_link")
def confirm_link_change(message):
    chat_id = message.chat.id
    course_id = user_states[chat_id][1]
    courses_found = session.query(Course).filter(Course.courseID == course_id).all()
    if not len(courses_found):
        bot.send_message(chat_id, "произошла ошибка. попробуйте снова")
    courses_found[0].course_access_url = message.text
    session.commit()
    bot.send_message(chat_id, "ссылка успешно изменена.")
    user_states.pop(chat_id)
