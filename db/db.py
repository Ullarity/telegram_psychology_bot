from sqlalchemy import create_engine, ForeignKey, String, Integer, Boolean, Column
from sqlalchemy.orm import sessionmaker, declarative_base
import uuid

Base = declarative_base()


def generate_uuid():
    return str(uuid.uuid4())

class User(Base):
    __tablename__ = "user"
    userID = Column("user_id", String, primary_key=True, default=generate_uuid)
    username = Column("username", String, unique=True, nullable=True)
    preferred_name = Column("preferred_name", String)
    chatID = Column("chatID", String)
    is_admin = Column("is_admin", Boolean, default=False)

    def __init__(self, username, preferred_name, chatID):
        self.username = username
        self.preferred_name = preferred_name
        self.chatID = chatID


class User_to_Course(Base):
    __tablename__ = "user_to_course"
    ID = Column("relation_id", String, primary_key=True, default=generate_uuid)
    userID = Column("user_id", ForeignKey("user.user_id"))
    courseID = Column("course_id", ForeignKey("course.course_id"))
    stopped_at = Column("stopped_at", Integer, default=0)
    has_paid = Column("has_paid", Boolean, default=False)

    def __init__(self, userID, courseID):
        self.userID = userID
        self.courseID = courseID


class Course(Base):
    __tablename__ = "course"
    courseID = Column("course_id", String, primary_key=True, default=generate_uuid)
    course_name = Column("course_name", String)
    course_price = Column("course_price", Integer)
    course_description = Column("course_description", String, nullable=True)
    course_access_url = Column("course_access_url", String, nullable=True)

    def __init__(self, course_name, course_price, course_description=None):
        self.course_name = course_name
        self.course_price = course_price
        if course_description:
            self.course_description = course_description



class Video(Base):
    __tablename__ = "video"
    videoID = Column("video_id", String, primary_key=True, default=generate_uuid)
    video_route = Column("video_route", String)
    video_name = Column("video_name", String)
    courseID = Column("course_id", ForeignKey("course.course_id"))
    order_in_course = Column("order_in_course", Integer, unique=True)


    def __init__(self, video_name, video_route, courseID, order_in_course):
        self.video_name = video_name
        self.video_route = video_route
        self.courseID = courseID
        self.order_in_course = order_in_course



db_url = "sqlite:///coursesDB.db"

engine = create_engine(db_url)
Base.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

