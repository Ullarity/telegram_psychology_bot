import re
import telebot
from services import is_admin

from db import session, User, Course, Video

from bot import bot, user_states, config

@bot.message_handler(commands=['start', 'help', 'начать'])
def welcome(message):
    print("Welcome handler")
    chat_id = message.chat.id
    users_found = session.query(User).filter(User.chatID == chat_id).all()
    if len(users_found):
        bot.send_message(chat_id, f"Здраствуйте, {users_found[0].preferred_name}", reply_markup=None)

        btn1 = telebot.types.InlineKeyboardButton("КУРС", callback_data="getCourse")
        markup = telebot.types.InlineKeyboardMarkup([[btn1]])

        text = 'Нажмите "Курс" чтобы продолжить'

        bot.send_message(chat_id,
                         text,
                         reply_markup=markup)


    else:
        bot.send_message(chat_id, "Как мне следует вас называть? (ваше имя).", reply_markup=None)
        user_states[chat_id] = ("registrationNamePrompt", "")


@bot.message_handler(func=lambda msg: msg.chat.id in user_states and user_states[msg.chat.id][0] == "registrationNamePrompt")
def register_new_user(message):
    print("register handler")
    chat_id = message.chat.id
    user_states.pop(chat_id)


    user = User(message.from_user.username, message.text, chat_id)
    session.add(user)
    session.commit()


    # btn1 = telebot.types.InlineKeyboardButton("посмотреть курсы", callback_data="seeCourses")
    # btn2 = telebot.types.InlineKeyboardButton("купить курсы", callback_data="buyCourses")

    btn1 = telebot.types.InlineKeyboardButton("КУРС", callback_data="getCourse")

    markup = telebot.types.InlineKeyboardMarkup([[btn1]])

    text = 'Нажмите "Курс" чтобы продолжить'

    bot.send_message(chat_id,
                     text,
                     reply_markup=markup)


@bot.message_handler(commands=['delete_account'])
def handle_account_deleting(message):
    print("account delete handler")
    chat_id = message.chat.id
    user_states[chat_id] = ("accountDeleteConfirmationRequired", "")
    bot.send_message(chat_id, "Вы уверены что хотите удалить ваш аккаунт? Все данные о купленных и пройденных вами курсах будут утеряны. \n Подтвердите написав да или нет.")



@bot.message_handler(func=lambda msg: msg.text.lower() in ['да', 'нет'] and msg.chat.id in user_states and user_states[msg.chat.id][0] == "accountDeleteConfirmationRequired")
def handle_account_deleting_confirmation(message):
    print("account delete confirmed handler")
    chat_id = message.chat.id
    if message.text == "да":
        users_found = session.query(User).filter(User.username == message.from_user.username).all()
        if len(users_found):
            session.delete(users_found[0])
            bot.send_message(chat_id, "Ваш аккаунт удалён.")
        else:
            bot.send_message(chat_id, "ERROR 404: User not found")
    else:
        bot.send_message(chat_id, "Удаление аккаунта отменено.")
    user_states.pop(chat_id)










@bot.message_handler(commands=['buy'])
def send_invoice(message):
    print("PRINT12")
    chat_id = message.chat.id

    bot.send_invoice(chat_id,
                     provider_token=config["PAYMENTS_TOKEN"],
                     title="course",
                     description="description",
                     invoice_payload="custom_payload",
                     currency="USD",
                     prices=[telebot.types.LabeledPrice("Start course", 1200)],
                     )